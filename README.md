## ASLRG ##

This is a game about growing a tree.

The player starts in a single node **the ROOT**.

You will be met with evil tree mites which you must kill.

After you kill them all, your tree will grow, and your quest continues.

How large can you grow your tree?

## Technical stuffs ##

This is a project to learn rust and create an ascii art game.